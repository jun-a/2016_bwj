require 'open-uri'
require 'nokogiri'
require 'json'
require 'csv'
require 'open-uri'


CSV.open("/Users/kabushikikaisharijobu/apps/bwj_share/crowdworks.csv", "wb") do |csv|
  base_url = 'http://crowdworks.jp/public/employees?utf8=%E2%9C%93&keywords=%E5%8F%96%E6%9D%90&order=&category=employees&path=occupation%2F22&form_request=1&keep_search_context=true'
  (1..157).size.times do |time|
  url = base_url + "&page=#{time}"
  html = open(url)
  doc = Nokogiri::HTML.parse(html)

  doc.css('.item.member_item').each do |i|
    puts count = i.css('span.count').inner_text #受注件数
    puts score = i.css('span.score').inner_text #評価
    i.css('ul.cw-list_inline').css('li').size.times do |time|
      puts i.css('ul.cw-list_inline').css('li')[time].inner_text.strip!
    end
    page_url = 'http://crowdworks.jp/' + i.css('.item_title').css('a').attribute('href').value
    page_html = open(page_url)
    page_doc = Nokogiri::HTML.parse(page_html)
    p name = page_doc.css('h1').inner_text
    profile_status = page_doc.css('.attributes').inner_text.split('/')
    p job_status = profile_status[0].gsub(/(\r\n|\r|\n)/, "") #業種形態
    p sex = profile_status[1] #性別
    p age = profile_status[2].split('(')[0].strip
    p locate =  profile_status[2].split('(')[1].split(')')[0].strip

    puts page_doc.css('.introduction p').inner_text

    csv << [name, page_url, job_status, sex, age, locate, count, score]
  end
  end
end
