require 'open-uri'
require 'nokogiri'
require 'date'
require "csv"
require 'json'
require 'open-uri'
require 'capybara'
require 'capybara/poltergeist'


def tokyo
  CSV.open("/Users/kabushikikaisharijobu/apps/bwj_share/townwork_kaigo_tokyo_22.csv", "wb") do |csv|
  csv << ["会社名",	"電話番号",	"都道府県",	"市区郡",	"姓",	"アウトorイン",	"リード状況",	"備考",	"リストランク",	"業種（選択）",	"Web サイト"]
  base_url = "https://townwork.net/tokyo/jc_015/jmc_01511/&page="
    (1..29).each do |number|
      url = base_url + number.to_s
      html = open(url)
      doc = Nokogiri::HTML.parse(html)
      doc.css('a.job-lst-main-box-inner.ico-job-lst-main-new-wrap').each do |a|
        puts detail_url = 'https://townwork.net' + a.attr('href')
        detail_html = open(detail_url)
        detail_doc = Nokogiri::HTML.parse(detail_html)
        company_name = ""
        remark = ""
        address = ""
        website = ""
        tel = ""
        rank =  "【介護用】「A」営利法人"
        detail_doc.search('dt').each_with_index do |d,idx|
          company_name = detail_doc.search('dd')[idx].inner_text.gsub(/(\r\n|\r|\n|\s|\f)/,"") if detail_doc.search('dt')[idx].inner_text == '社名（店舗名）'
          remark = detail_doc.search('dd')[idx].inner_text.gsub(/(\r\n|\r|\n|\s|\f)/,"") + '・タウンワーク'  if detail_doc.search('dt')[idx].inner_text == '会社事業内容'
          address = detail_doc.search('dd')[idx].inner_text.gsub(/(\r\n|\r|\n|\s|\f)/,"").delete('東京都')  if detail_doc.search('dt')[idx].inner_text == '会社住所'
          website = detail_doc.search('dd')[idx].inner_text.gsub(/(\r\n|\r|\n|\s|\f)/,"")  if detail_doc.search('dt')[idx].inner_text == 'ホームページリンク'
          tel = detail_doc.css('li.detail-tel-box.jsc-phone-popup p.detail-tel-ttl')[0].inner_text.gsub(/(\r\n|\r|\n|\s|\f)/,"").split("\n")[0]
          #tel = tel.split('-')
          #tel = tel[0] + '-' + tel[1] + '-' + tel[2]
          #puts detail_doc.search('dt')[idx].inner_text
        end
        #電話番号引っこ抜く
        if tel.split('-')[0] == ('070' || '090' || '080' || '050')
          n = 13
        else
          n = 12
        end
        remark = remark + website
        # puts tel.slice!(1...n)
        tel = tel.delete("^0-9")
        fix_tel = tel.scan(/.{1,#{n}}/)[0]
        rank =  "【介護用】「D」医療法人・公営" if (company_name.include?('医療法人') || company_name.include?('病院') || company_name.include?('立') || company_name.include?('医院'))
        rank =  "【介護用】「C」社会福祉法人" if company_name.include?('社会福祉法人')
        rank =  "【介護用】「B」NPO法人・一般社団法人" if (company_name.include?('特定') || company_name.include?('NPO'))
        rank =  "【介護用】「A」営利法人" if (company_name.include?('会社') || company_name.include?('株'))
        # puts company_name
        # puts remark
        # puts address
        # puts website
        # puts fix_tel
        # puts rank
        csv << [company_name, fix_tel, "東京", address, "なし", "アウト","架電可", remark,rank,"介護",detail_url]
          # puts company_name = table[n].css('dd').inner_text  if table[n].css('dt') == '社名（店舗名）'
          # puts remark = table[n].css('dd').inner_text  if table[n].css('dt') == '会社事業内容'
          # puts address = table[n].css('dd').inner_text  if table[n].css('dt') == '会社住所'
          # puts website = table[n].css('dd').inner_text  if table[n].css('dt') == 'ホームページリンク'
        #end
        #if table.css('')
        #puts company_name = table.css('dd')[0].inner_text
        # puts remark = table.css('dd')[1].inner_text
        # puts address = table.css('dd')[2].inner_text
        # puts website = table.css('dd')[3].inner_text
      end
      sleep(1)
    end
  end
end


def kanagawa
  CSV.open("/Users/kabushikikaisharijobu/apps/bwj_share/townwork_kaigo_kanagawa.csv", "wb") do |csv|
  csv << ["会社名",	"電話番号",	"都道府県",	"市区郡",	"姓",	"アウトorイン",	"リード状況",	"備考",	"リストランク",	"業種（選択）",	"Web サイト"]
  base_url = "https://townwork.net/joSrchRsltList/?emc=01&emc=06&emc=04&emc=03&ac=042&jmc=01511&jmc=01506&jmc=01510&jmc=01512&jmc=01514&jmc=01515&jmc=01517&jmc=01518&page="
    (1..46).each do |number|
      url = base_url + number.to_s
      html = open(url)
      doc = Nokogiri::HTML.parse(html)
      doc.css('a.job-lst-main-box-inner.ico-job-lst-main-new-wrap').each do |a|
        puts detail_url = 'https://townwork.net' + a.attr('href')
        detail_html = open(detail_url)
        detail_doc = Nokogiri::HTML.parse(detail_html)
        company_name = ""
        remark = ""
        address = ""
        website = ""
        tel = ""
        rank =  "【介護用】「A」営利法人"
        detail_doc.search('dt').each_with_index do |d,idx|
          company_name = detail_doc.search('dd')[idx].inner_text.gsub(/(\r\n|\r|\n|\s|\f)/,"") if detail_doc.search('dt')[idx].inner_text == '社名（店舗名）'
          remark = detail_doc.search('dd')[idx].inner_text.gsub(/(\r\n|\r|\n|\s|\f)/,"") + '・タウンワーク'  if detail_doc.search('dt')[idx].inner_text == '会社事業内容'
          address = detail_doc.search('dd')[idx].inner_text.gsub(/(\r\n|\r|\n|\s|\f)/,"").delete('神奈川県')  if detail_doc.search('dt')[idx].inner_text == '会社住所'
          website = detail_doc.search('dd')[idx].inner_text.gsub(/(\r\n|\r|\n|\s|\f)/,"")  if detail_doc.search('dt')[idx].inner_text == 'ホームページリンク'
          tel = detail_doc.css('li.detail-tel-box.jsc-phone-popup p.detail-tel-ttl')[0].inner_text.gsub(/(\r\n|\r|\n|\s|\f)/,"").split("\n")[0]
          #tel = tel.split('-')
          #tel = tel[0] + '-' + tel[1] + '-' + tel[2]
          #puts detail_doc.search('dt')[idx].inner_text
        end
        #電話番号引っこ抜く
        if tel.split('-')[0] == ('070' || '090' || '080' || '050')
          n = 13
        else
          n = 12
        end
        remark = remark + website
        # puts tel.slice!(1...n)
        tel = tel.delete("^0-9")
        fix_tel = tel.scan(/.{1,#{n}}/)[0]
        rank =  "【介護用】「D」医療法人・公営" if (company_name.include?('医療法人') || company_name.include?('病院') || company_name.include?('立') || company_name.include?('医院'))
        rank =  "【介護用】「C」社会福祉法人" if company_name.include?('社会福祉法人')
        rank =  "【介護用】「B」NPO法人・一般社団法人" if (company_name.include?('特定') || company_name.include?('NPO'))
        rank =  "【介護用】「A」営利法人" if (company_name.include?('会社') || company_name.include?('株'))
        # puts company_name
        # puts remark
        # puts address
        # puts website
        # puts fix_tel
        # puts rank
        csv << [company_name, fix_tel, "神奈川", address, "なし", "アウト","架電可", remark,rank,"介護",detail_url]
          # puts company_name = table[n].css('dd').inner_text  if table[n].css('dt') == '社名（店舗名）'
          # puts remark = table[n].css('dd').inner_text  if table[n].css('dt') == '会社事業内容'
          # puts address = table[n].css('dd').inner_text  if table[n].css('dt') == '会社住所'
          # puts website = table[n].css('dd').inner_text  if table[n].css('dt') == 'ホームページリンク'
        #end
        #if table.css('')
        #puts company_name = table.css('dd')[0].inner_text
        # puts remark = table.css('dd')[1].inner_text
        # puts address = table.css('dd')[2].inner_text
        # puts website = table.css('dd')[3].inner_text
      end
      sleep(1)
    end
  end
end
def saitama
  CSV.open("/Users/kabushikikaisharijobu/apps/bwj_share/townwork_kaigo_saitama.csv", "wb") do |csv|
  csv << ["会社名",	"電話番号",	"都道府県",	"市区郡",	"姓",	"アウトorイン",	"リード状況",	"備考",	"リストランク",	"業種（選択）",	"Web サイト"]
  base_url = "https://townwork.net/joSrchRsltList/?emc=01&emc=06&emc=04&emc=03&ac=044&jmc=01511&jmc=01506&jmc=01510&jmc=01512&jmc=01514&jmc=01515&jmc=01517&jmc=01518&page="
    (1..59).each do |number|
      url = base_url + number.to_s
      html = open(url)
      doc = Nokogiri::HTML.parse(html)
      doc.css('a.job-lst-main-box-inner.ico-job-lst-main-new-wrap').each do |a|
        puts detail_url = 'https://townwork.net' + a.attr('href')
        detail_html = open(detail_url)
        detail_doc = Nokogiri::HTML.parse(detail_html)
        company_name = ""
        remark = ""
        address = ""
        website = ""
        tel = ""
        rank =  "【介護用】「A」営利法人"
        detail_doc.search('dt').each_with_index do |d,idx|
          company_name = detail_doc.search('dd')[idx].inner_text.gsub(/(\r\n|\r|\n|\s|\f)/,"") if detail_doc.search('dt')[idx].inner_text == '社名（店舗名）'
          remark = detail_doc.search('dd')[idx].inner_text.gsub(/(\r\n|\r|\n|\s|\f)/,"") + '・タウンワーク'  if detail_doc.search('dt')[idx].inner_text == '会社事業内容'
          address = detail_doc.search('dd')[idx].inner_text.gsub(/(\r\n|\r|\n|\s|\f)/,"").delete('埼玉県')  if detail_doc.search('dt')[idx].inner_text == '会社住所'
          website = detail_doc.search('dd')[idx].inner_text.gsub(/(\r\n|\r|\n|\s|\f)/,"")  if detail_doc.search('dt')[idx].inner_text == 'ホームページリンク'
          tel = detail_doc.css('li.detail-tel-box.jsc-phone-popup p.detail-tel-ttl')[0].inner_text.gsub(/(\r\n|\r|\n|\s|\f)/,"").split("\n")[0]
          #tel = tel.split('-')
          #tel = tel[0] + '-' + tel[1] + '-' + tel[2]
          #puts detail_doc.search('dt')[idx].inner_text
        end
        #電話番号引っこ抜く
        if tel.split('-')[0] == ('070' || '090' || '080' || '050')
          n = 13
        else
          n = 12
        end
        remark = remark + website
        # puts tel.slice!(1...n)
        tel = tel.delete("^0-9")
        fix_tel = tel.scan(/.{1,#{n}}/)[0]
        rank =  "【介護用】「D」医療法人・公営" if (company_name.include?('医療法人') || company_name.include?('病院') || company_name.include?('立') || company_name.include?('医院'))
        rank =  "【介護用】「C」社会福祉法人" if company_name.include?('社会福祉法人')
        rank =  "【介護用】「B」NPO法人・一般社団法人" if (company_name.include?('特定') || company_name.include?('NPO'))
        rank =  "【介護用】「A」営利法人" if (company_name.include?('会社') || company_name.include?('株'))
        # puts company_name
        # puts remark
        # puts address
        # puts website
        # puts fix_tel
        # puts rank
        csv << [company_name, fix_tel, "埼玉", address, "なし", "アウト","架電可", remark,rank,"介護",detail_url]
          # puts company_name = table[n].css('dd').inner_text  if table[n].css('dt') == '社名（店舗名）'
          # puts remark = table[n].css('dd').inner_text  if table[n].css('dt') == '会社事業内容'
          # puts address = table[n].css('dd').inner_text  if table[n].css('dt') == '会社住所'
          # puts website = table[n].css('dd').inner_text  if table[n].css('dt') == 'ホームページリンク'
        #end
        #if table.css('')
        #puts company_name = table.css('dd')[0].inner_text
        # puts remark = table.css('dd')[1].inner_text
        # puts address = table.css('dd')[2].inner_text
        # puts website = table.css('dd')[3].inner_text
      end
      sleep(1)
    end
  end
end

def chiba
  CSV.open("/Users/kabushikikaisharijobu/apps/bwj_share/townwork_kaigo_chiba.csv", "wb") do |csv|
  csv << ["会社名",	"電話番号",	"都道府県",	"市区郡",	"姓",	"アウトorイン",	"リード状況",	"備考",	"リストランク",	"業種（選択）",	"Web サイト"]
  base_url = "https://townwork.net/joSrchRsltList/?emc=01&emc=06&emc=04&emc=03&ac=043&jmc=01511&jmc=01506&jmc=01510&jmc=01512&jmc=01514&jmc=01515&jmc=01517&jmc=01518&page="
    (1..40).each do |number|
      url = base_url + number.to_s
      html = open(url)
      doc = Nokogiri::HTML.parse(html)
      doc.css('a.job-lst-main-box-inner.ico-job-lst-main-new-wrap').each do |a|
        puts detail_url = 'https://townwork.net' + a.attr('href')
        detail_html = open(detail_url)
        detail_doc = Nokogiri::HTML.parse(detail_html)
        company_name = ""
        remark = ""
        address = ""
        website = ""
        tel = ""
        rank =  "【介護用】「A」営利法人"
        detail_doc.search('dt').each_with_index do |d,idx|
          company_name = detail_doc.search('dd')[idx].inner_text.gsub(/(\r\n|\r|\n|\s|\f)/,"") if detail_doc.search('dt')[idx].inner_text == '社名（店舗名）'
          remark = detail_doc.search('dd')[idx].inner_text.gsub(/(\r\n|\r|\n|\s|\f)/,"") + '・タウンワーク'  if detail_doc.search('dt')[idx].inner_text == '会社事業内容'
          address = detail_doc.search('dd')[idx].inner_text.gsub(/(\r\n|\r|\n|\s|\f)/,"").delete('千葉県')  if detail_doc.search('dt')[idx].inner_text == '会社住所'
          website = detail_doc.search('dd')[idx].inner_text.gsub(/(\r\n|\r|\n|\s|\f)/,"")  if detail_doc.search('dt')[idx].inner_text == 'ホームページリンク'
          tel = detail_doc.css('li.detail-tel-box.jsc-phone-popup p.detail-tel-ttl')[0].inner_text.gsub(/(\r\n|\r|\n|\s|\f)/,"").split("\n")[0]
          #tel = tel.split('-')
          #tel = tel[0] + '-' + tel[1] + '-' + tel[2]
          #puts detail_doc.search('dt')[idx].inner_text
        end
        #電話番号引っこ抜く
        if tel.split('-')[0] == ('070' || '090' || '080' || '050')
          n = 13
        else
          n = 12
        end
        remark = remark + website
        # puts tel.slice!(1...n)
        tel = tel.delete("^0-9")
        fix_tel = tel.scan(/.{1,#{n}}/)[0]
        rank =  "【介護用】「D」医療法人・公営" if (company_name.include?('医療法人') || company_name.include?('病院') || company_name.include?('立') || company_name.include?('医院'))
        rank =  "【介護用】「C」社会福祉法人" if company_name.include?('社会福祉法人')
        rank =  "【介護用】「B」NPO法人・一般社団法人" if (company_name.include?('特定') || company_name.include?('NPO'))
        rank =  "【介護用】「A」営利法人" if (company_name.include?('会社') || company_name.include?('株'))
        # puts company_name
        # puts remark
        # puts address
        # puts website
        # puts fix_tel
        # puts rank
        csv << [company_name, fix_tel, "千葉", address, "なし", "アウト","架電可", remark,rank,"介護",detail_url]
          # puts company_name = table[n].css('dd').inner_text  if table[n].css('dt') == '社名（店舗名）'
          # puts remark = table[n].css('dd').inner_text  if table[n].css('dt') == '会社事業内容'
          # puts address = table[n].css('dd').inner_text  if table[n].css('dt') == '会社住所'
          # puts website = table[n].css('dd').inner_text  if table[n].css('dt') == 'ホームページリンク'
        #end
        #if table.css('')
        #puts company_name = table.css('dd')[0].inner_text
        # puts remark = table.css('dd')[1].inner_text
        # puts address = table.css('dd')[2].inner_text
        # puts website = table.css('dd')[3].inner_text
      end
      sleep(1)
    end
  end
end

tokyo
# chiba
# saitama
# kanagawa
