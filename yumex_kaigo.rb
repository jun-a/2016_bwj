require 'open-uri'
require 'nokogiri'
require 'date'
require "csv"
require 'json'
require 'open-uri'
require 'capybara'
require 'capybara/poltergeist'

def tokyo
  # csv << [company_name, fix_tel, "東京", address, "なし", "アウト","架電可", remark,rank,"介護",detail_url]
  CSV.open("/Users/kabushikikaisharijobu/apps/bwj_share/yumex_tokyo.csv", "wb") do |csv|
  csv << ["会社名",	"電話番号",	"都道府県",	"市区郡",	"姓",	"アウトorイン",	"リード状況",	"備考",	"リストランク",	"業種（選択）",	"Web サイト"]

    base_url = 'http://yumexnet.jp/kanto/shokushu_6_01_'
    (1..5).each do |number|
      begin
        puts url = base_url + number.to_s + '.html'
        html = open(url)
        doc = Nokogiri::HTML.parse(html)
        doc.css('.aboutjob').each do |a|
          puts detail_url = 'http://yumexnet.jp/kanto/' + a.attr('href')
          detail_html = open(detail_url)
          detail_doc = Nokogiri::HTML.parse(detail_html)
          company_name = ""
          address = ""
          fix_tel = ""
          rank = ""
          detail_doc.css('.condition td').each_with_index do |o,idx|
            puts company_name = o.inner_text.gsub(/(\r\n|\r|\n|\s|\f)/,"") if detail_doc.css('.condition th')[idx].inner_text.include?('社名')
            puts address = o.inner_text.gsub(/(\r\n|\r|\n|\s|\f)/,"").delete('東京都') if detail_doc.css('.condition th')[idx].inner_text.include?('マップ住所')
            puts fix_tel = o.inner_text.gsub(/(\r\n|\r|\n|\s|\f)/,"").split('（')[0] if detail_doc.css('.condition th')[idx].inner_text.include?('連絡先')
          end
          rank =  "【介護用】「D」医療法人・公営" if (company_name.include?('医療法人') || company_name.include?('病院') || company_name.include?('立') || company_name.include?('医院'))
          rank =  "【介護用】「C」社会福祉法人" if company_name.include?('社会福祉法人')
          rank =  "【介護用】「B」NPO法人・一般社団法人" if (company_name.include?('特定') || company_name.include?('NPO'))
          rank =  "【介護用】「A」営利法人" if (company_name.include?('会社') || company_name.include?('株'))
          csv << [company_name, fix_tel, "東京", address, "なし", "アウト","架電可", "",rank,"介護",detail_url]
          sleep(1)
        end
      rescue => e
        puts url + " エラーが発生しました。"
        puts e
        next
      end
    end
  end
end

def tokyo_city
  # csv << [company_name, fix_tel, "東京", address, "なし", "アウト","架電可", remark,rank,"介護",detail_url]
  CSV.open("/Users/kabushikikaisharijobu/apps/bwj_share/yumex_tokyo_city.csv", "wb") do |csv|
  csv << ["会社名",	"電話番号",	"都道府県",	"市区郡",	"姓",	"アウトorイン",	"リード状況",	"備考",	"リストランク",	"業種（選択）",	"Web サイト"]

    base_url = 'http://yumexnet.jp/kanto/shokushu_6_02_'
    (1..4).each do |number|
      begin
        puts url = base_url + number.to_s + '.html'
        html = open(url)
        doc = Nokogiri::HTML.parse(html)
        doc.css('.aboutjob').each do |a|
          puts detail_url = 'http://yumexnet.jp/kanto/' + a.attr('href')
          detail_html = open(detail_url)
          detail_doc = Nokogiri::HTML.parse(detail_html)
          company_name = ""
          address = ""
          fix_tel = ""
          rank = ""
          detail_doc.css('.condition td').each_with_index do |o,idx|
            puts company_name = o.inner_text.gsub(/(\r\n|\r|\n|\s|\f)/,"") if detail_doc.css('.condition th')[idx].inner_text.include?('社名')
            puts address = o.inner_text.gsub(/(\r\n|\r|\n|\s|\f)/,"").delete('東京都') if detail_doc.css('.condition th')[idx].inner_text.include?('マップ住所')
            puts fix_tel = o.inner_text.gsub(/(\r\n|\r|\n|\s|\f)/,"").split('（')[0] if detail_doc.css('.condition th')[idx].inner_text.include?('連絡先')
          end
          rank =  "【介護用】「D」医療法人・公営" if (company_name.include?('医療法人') || company_name.include?('病院') || company_name.include?('立') || company_name.include?('医院'))
          rank =  "【介護用】「C」社会福祉法人" if company_name.include?('社会福祉法人')
          rank =  "【介護用】「B」NPO法人・一般社団法人" if (company_name.include?('特定') || company_name.include?('NPO'))
          rank =  "【介護用】「A」営利法人" if (company_name.include?('会社') || company_name.include?('株'))
          csv << [company_name, fix_tel, "東京", address, "なし", "アウト","架電可", "",rank,"介護",detail_url]
          sleep(1)
        end
      rescue => e
        puts url + " エラーが発生しました。"
        puts e
        next
      end
    end
  end
end
def kanagawa
  # csv << [company_name, fix_tel, "東京", address, "なし", "アウト","架電可", remark,rank,"介護",detail_url]
  CSV.open("/Users/kabushikikaisharijobu/apps/bwj_share/yumex_kanagawa.csv", "wb") do |csv|
  csv << ["会社名",	"電話番号",	"都道府県",	"市区郡",	"姓",	"アウトorイン",	"リード状況",	"備考",	"リストランク",	"業種（選択）",	"Web サイト"]

    base_url = 'http://yumexnet.jp/kanto/shokushu_6_03_'
    (1..7).each do |number|
      begin
        puts url = base_url + number.to_s + '.html'
        html = open(url)
        doc = Nokogiri::HTML.parse(html)
        doc.css('.aboutjob').each do |a|
          puts detail_url = 'http://yumexnet.jp/kanto/' + a.attr('href')
          detail_html = open(detail_url)
          detail_doc = Nokogiri::HTML.parse(detail_html)
          company_name = ""
          address = ""
          fix_tel = ""
          rank = ""
          detail_doc.css('.condition td').each_with_index do |o,idx|
            puts company_name = o.inner_text.gsub(/(\r\n|\r|\n|\s|\f)/,"") if detail_doc.css('.condition th')[idx].inner_text.include?('社名')
            puts address = o.inner_text.gsub(/(\r\n|\r|\n|\s|\f)/,"").delete('神奈川県') if detail_doc.css('.condition th')[idx].inner_text.include?('マップ住所')
            puts fix_tel = o.inner_text.gsub(/(\r\n|\r|\n|\s|\f)/,"").split('（')[0] if detail_doc.css('.condition th')[idx].inner_text.include?('連絡先')
          end
          rank =  "【介護用】「D」医療法人・公営" if (company_name.include?('医療法人') || company_name.include?('病院') || company_name.include?('立') || company_name.include?('医院'))
          rank =  "【介護用】「C」社会福祉法人" if company_name.include?('社会福祉法人')
          rank =  "【介護用】「B」NPO法人・一般社団法人" if (company_name.include?('特定') || company_name.include?('NPO'))
          rank =  "【介護用】「A」営利法人" if (company_name.include?('会社') || company_name.include?('株'))
          csv << [company_name, fix_tel, "神奈川", address, "なし", "アウト","架電可", "",rank,"介護",detail_url]
          sleep(1)
        end
      rescue => e
        puts url + " エラーが発生しました。"
        puts e
        next
      end
    end
  end
end

def saitama
  # csv << [company_name, fix_tel, "東京", address, "なし", "アウト","架電可", remark,rank,"介護",detail_url]
  CSV.open("/Users/kabushikikaisharijobu/apps/bwj_share/yumex_saitama.csv", "wb") do |csv|
  csv << ["会社名",	"電話番号",	"都道府県",	"市区郡",	"姓",	"アウトorイン",	"リード状況",	"備考",	"リストランク",	"業種（選択）",	"Web サイト"]

    base_url = 'http://yumexnet.jp/kanto/shokushu_6_04_'
    (1..6).each do |number|
      begin
        puts url = base_url + number.to_s + '.html'
        html = open(url)
        doc = Nokogiri::HTML.parse(html)
        doc.css('.aboutjob').each do |a|
          puts detail_url = 'http://yumexnet.jp/kanto/' + a.attr('href')
          detail_html = open(detail_url)
          detail_doc = Nokogiri::HTML.parse(detail_html)
          company_name = ""
          address = ""
          fix_tel = ""
          rank = ""
          detail_doc.css('.condition td').each_with_index do |o,idx|
            puts company_name = o.inner_text.gsub(/(\r\n|\r|\n|\s|\f)/,"") if detail_doc.css('.condition th')[idx].inner_text.include?('社名')
            puts address = o.inner_text.gsub(/(\r\n|\r|\n|\s|\f)/,"").delete('埼玉県') if detail_doc.css('.condition th')[idx].inner_text.include?('マップ住所')
            puts fix_tel = o.inner_text.gsub(/(\r\n|\r|\n|\s|\f)/,"").split('（')[0] if detail_doc.css('.condition th')[idx].inner_text.include?('連絡先')
          end
          rank =  "【介護用】「D」医療法人・公営" if (company_name.include?('医療法人') || company_name.include?('病院') || company_name.include?('立') || company_name.include?('医院'))
          rank =  "【介護用】「C」社会福祉法人" if company_name.include?('社会福祉法人')
          rank =  "【介護用】「B」NPO法人・一般社団法人" if (company_name.include?('特定') || company_name.include?('NPO'))
          rank =  "【介護用】「A」営利法人" if (company_name.include?('会社') || company_name.include?('株'))
          csv << [company_name, fix_tel, "埼玉", address, "なし", "アウト","架電可", "",rank,"介護",detail_url]
          sleep(1)
        end
      rescue => e
        puts url + " エラーが発生しました。"
        puts e
        next
      end
    end
  end
end
def chiba
  # csv << [company_name, fix_tel, "東京", address, "なし", "アウト","架電可", remark,rank,"介護",detail_url]
  CSV.open("/Users/kabushikikaisharijobu/apps/bwj_share/yumex_chiba.csv", "wb") do |csv|
  csv << ["会社名",	"電話番号",	"都道府県",	"市区郡",	"姓",	"アウトorイン",	"リード状況",	"備考",	"リストランク",	"業種（選択）",	"Web サイト"]

    base_url = 'http://yumexnet.jp/kanto/shokushu_6_05_'
    (1..4).each do |number|
      begin
        puts url = base_url + number.to_s + '.html'
        html = open(url)
        doc = Nokogiri::HTML.parse(html)
        doc.css('.aboutjob').each do |a|
          puts detail_url = 'http://yumexnet.jp/kanto/' + a.attr('href')
          detail_html = open(detail_url)
          detail_doc = Nokogiri::HTML.parse(detail_html)
          company_name = ""
          address = ""
          fix_tel = ""
          rank = ""
          detail_doc.css('.condition td').each_with_index do |o,idx|
            puts company_name = o.inner_text.gsub(/(\r\n|\r|\n|\s|\f)/,"") if detail_doc.css('.condition th')[idx].inner_text.include?('社名')
            puts address = o.inner_text.gsub(/(\r\n|\r|\n|\s|\f)/,"").delete('千葉県') if detail_doc.css('.condition th')[idx].inner_text.include?('マップ住所')
            puts fix_tel = o.inner_text.gsub(/(\r\n|\r|\n|\s|\f)/,"").split('（')[0] if detail_doc.css('.condition th')[idx].inner_text.include?('連絡先')
          end
          rank =  "【介護用】「D」医療法人・公営" if (company_name.include?('医療法人') || company_name.include?('病院') || company_name.include?('立') || company_name.include?('医院'))
          rank =  "【介護用】「C」社会福祉法人" if company_name.include?('社会福祉法人')
          rank =  "【介護用】「B」NPO法人・一般社団法人" if (company_name.include?('特定') || company_name.include?('NPO'))
          rank =  "【介護用】「A」営利法人" if (company_name.include?('会社') || company_name.include?('株'))
          csv << [company_name, fix_tel, "千葉", address, "なし", "アウト","架電可", "",rank,"介護",detail_url]
          sleep(1)
        end
      rescue => e
        puts url + " エラーが発生しました。"
        puts e
        next
      end
    end
  end
end




#tokyo
tokyo_city
saitama
kanagawa
chiba
