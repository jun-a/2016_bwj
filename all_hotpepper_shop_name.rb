require 'open-uri'
require 'nokogiri'
require 'date'
require "csv"
require 'json'
require 'open-uri'


def hair_hotpapper
  # prefectures = {pre01:"北海道",pre02:"青森県",pre03:"岩手県",pre04:"宮城県",pre05:"秋田県",pre06:"山形県",pre07:"福島県",pre08:"茨城県",pre09:"栃木県",pre10:"群馬県",pre11:"埼玉県",pre12:"千葉県",pre13:"東京都",pre14:"神奈川県",pre15:"新潟県",pre16:"富山県",pre17:"石川県",pre18:"福井県",pre19:"山梨県",pre20:"長野県",pre21:"岐阜県",pre22:"静岡県",pre23:"愛知県",pre24:"三重県",pre25:"滋賀県",pre26:"京都府",pre27:"大阪府",pre28:"兵庫県",pre29:"奈良県",pre30:"和歌山県",pre31:"鳥取県",pre32:"島根県",pre33:"岡山県",pre34:"広島県",pre35:"山口県",pre36:"徳島県",pre37:"香川県",pre38:"愛媛県",pre39:"高知県",pre40:"福岡県",pre41:"佐賀県",pre42:"長崎県",pre43:"熊本県",pre44:"大分県",pre45:"宮崎県",pre46:"鹿児島県",pre47:"沖縄県"}

  prefectures = {pre13:"東京都"}
  CSV.open("/Users/kabushikikaisharijobu/apps/bwj_share/all_hotpepper/shop_name_hair_hotpapper.csv", "wb") do |csv|
    prefectures.each do |key,value|
      csv << ["会社名","URL","都道府県"]

      puts main_url = "https://beauty.hotpepper.jp/#{key}/"
      base_html = open(main_url)
      base_doc = Nokogiri::HTML.parse(base_html)
      puts max_page_number = base_doc.css('p.pa.bottom0.right0').inner_text.split('/')[1].split('ページ')[0].to_i

      base_url = "https://beauty.hotpepper.jp/#{key}/PN"
      (1..max_page_number).each do |number|
        sleep(1)
        url = base_url + number.to_s + '.html'
        html = open(url)
        doc = Nokogiri::HTML.parse(html)
        doc.css('h3.slcHead.cFix a').each do |a|
        begin
          open_url = a.attribute('href').value.split('/?cstt=')[0]
          open_html = open(open_url)
          open_doc = Nokogiri::HTML.parse(open_html)
          shop_name = open_doc.css('p.detailTitle').inner_text#.split#("\n")[1].strip! #店舗名
          csv << [shop_name,open_url,value]
        rescue => e
          next
        end
        end
      end
    end
  end
end

hair_hotpapper
