require 'open-uri'
require 'nokogiri'
require 'date'
require "csv"
require 'json'
# require 'capybara'
# require 'capybara/poltergeist'

def townwork
  CSV.open("/Users/kabushikikaisharijobu/apps/bwj_share/townwork_count.csv", "wb") do |csv|
    csv << ["会社名",	"電話番号",	"都道府県",	"市区郡",	"町名・番地",	"アウトorイン",	"リード状況",	"備考",	"リストランク",	"業種（選択）",	"Web サイト"]
    prefecture_names = ['hokkaidou','aomori','iwate','miyagi','akita','yamagata','fukushima','ibaraki','tochigi','gunma','saitama','chiba','tokyo','kanagawa','niigata','toyama','ishikawa','fukui','yamanashi','nagano','gifu','shizuoka','aichi','mie','shiga','kyoto','osaka','hyogo','nara','wakayama','tottori','shimane','okayama','hiroshima','yamaguchi','tokushima','kagawa','ehime','kouchi','fukuoka','saga','nagasaki','kumamoto','ooita','miyazaki','kagoshima','okinawa']
    #csv << ["会社名", "掲載期間", "電話番号",  "住所"]
      begin
      prefecture_names.each do |prefecture_name|

        puts base_url = 'https://townwork.net/' + prefecture_name + '/jc_017/jmc_01701/'

        original_html = open(base_url)
        original_doc = Nokogiri::HTML.parse(original_html)
        #ページの最終地点をとってくる
        size_number  = original_doc.css('.pager-number li').size - 1
        puts max_page_number = original_doc.css('.pager-number li')[size_number].inner_text.to_i

        (1..1).each do |number|
            url = base_url + '?page=' + number.to_s
            html = open(url)
            doc = Nokogiri::HTML.parse(html)
            doc.css('a.job-lst-main-box-inner.ico-job-lst-main-new-wrap').each do |a|
              open_url = 'https://townwork.net' +  a.attribute('href').value
              open_html = open(open_url)
              open_doc = Nokogiri::HTML.parse(open_html)
              shop_name = open_doc.css('h3.job-detail-ttl-txt').inner_text.split("\n")[2].strip! #店舗名
              pub_period = open_doc.css('.job-age-txt.job-detail-remaining-date-age p').inner_text.strip! #掲載期間
              tel = open_doc.css('p.detail-tel-ttl').inner_text.split("\n")[2].strip! if !open_doc.css.nil? #電話番号
              out_put_tel = tel.split("　")[0].strip if !open_doc.css.nil? #電話番号
              #アドレス出し
              address = ""
              open_doc.css('.job-ditail-tbl-inner').each do |ad|
                #puts ad.css('dt').inner_text
                address = ad.css('  dd').inner_text if ad.css('dt').inner_text == '会社住所'
              end
              address_first = open_doc.css('.breadcrumbs-wrap span')[1].inner_text
              address_second = open_doc.css('.breadcrumbs-wrap span')[2].inner_text.lstrip
              #csv << [shop_name, pub_period, tel, address]
              csv << [shop_name, out_put_tel, address_first,  address_second, address,  "アウト", "架電可", tel, "","美容師",open_url]
            end
          sleep(1)
        end
      end
      rescue => e
        puts e
        next
      end
  end
end


def hair_make_townwork
  CSV.open("/Users/kabushikikaisharijobu/apps/bwj_share/hair_make_townwork_count.csv", "wb") do |csv|
    csv << ["会社名",	"電話番号",	"都道府県",	"市区郡",	"町名・番地",	"アウトorイン",	"リード状況",	"備考",	"リストランク",	"業種（選択）",	"Web サイト"]
    prefecture_names = ['hokkaidou','aomori','iwate','miyagi','akita','yamagata','fukushima','ibaraki','tochigi','gunma','saitama','chiba','tokyo','kanagawa','niigata','toyama','ishikawa','fukui','yamanashi','nagano','gifu','shizuoka','aichi','mie','shiga','kyoto','osaka','hyogo','nara','wakayama','tottori','shimane','okayama','hiroshima','yamaguchi','tokushima','kagawa','ehime','kouchi','fukuoka','saga','nagasaki','kumamoto','ooita','miyazaki','kagoshima','okinawa']
    #csv << ["会社名", "掲載期間", "電話番号",  "住所"]
      begin
      prefecture_names.each do |prefecture_name|

        puts base_url = 'https://townwork.net/' + prefecture_name + '/jc_017/jmc_01704/'

        original_html = open(base_url)
        original_doc = Nokogiri::HTML.parse(original_html)
        #ページの最終地点をとってくる
        size_number  = original_doc.css('.pager-number li').size - 1
        puts max_page_number = original_doc.css('.pager-number li')[size_number].inner_text.to_i

        (1..max_page_number).each do |number|
            url = base_url + '?page=' + number.to_s
            html = open(url)
            doc = Nokogiri::HTML.parse(html)
            doc.css('a.job-lst-main-box-inner.ico-job-lst-main-new-wrap').each do |a|
              open_url = 'https://townwork.net' +  a.attribute('href').value
              open_html = open(open_url)
              open_doc = Nokogiri::HTML.parse(open_html)
              shop_name = open_doc.css('h3.job-detail-ttl-txt').inner_text.split("\n")[2].strip! #店舗名
              pub_period = open_doc.css('.job-age-txt.job-detail-remaining-date-age p').inner_text.strip! #掲載期間
              tel = open_doc.css('p.detail-tel-ttl').inner_text.split("\n")[2].strip! if !open_doc.css.nil? #電話番号
              out_put_tel = tel.split("　")[0].strip if !open_doc.css.nil? #電話番号
              #アドレス出し
              address = ""
              open_doc.css('.job-ditail-tbl-inner').each do |ad|
                #puts ad.css('dt').inner_text
                address = ad.css('  dd').inner_text if ad.css('dt').inner_text == '会社住所'
              end
              address_first = open_doc.css('.breadcrumbs-wrap span')[1].inner_text
              address_second = open_doc.css('.breadcrumbs-wrap span')[2].inner_text.lstrip
              #csv << [shop_name, pub_period, tel, address]
              csv << [shop_name, out_put_tel, address_first,  address_second, address,  "アウト", "架電可", tel, "","美容師",open_url]
            end
          sleep(1)
        end
      end
      rescue => e
        puts e
        next
      end
  end
end



def riyoshi_townwork
  CSV.open("/Users/kabushikikaisharijobu/apps/bwj_share/riyoshi_townwork_count.csv", "wb") do |csv|
    csv << ["会社名",	"電話番号",	"都道府県",	"市区郡",	"町名・番地",	"アウトorイン",	"リード状況",	"備考",	"リストランク",	"業種（選択）",	"Web サイト"]
    prefecture_names = ['hokkaidou','aomori','iwate','miyagi','akita','yamagata','fukushima','ibaraki','tochigi','gunma','saitama','chiba','tokyo','kanagawa','niigata','toyama','ishikawa','fukui','yamanashi','nagano','gifu','shizuoka','aichi','mie','shiga','kyoto','osaka','hyogo','nara','wakayama','tottori','shimane','okayama','hiroshima','yamaguchi','tokushima','kagawa','ehime','kouchi','fukuoka','saga','nagasaki','kumamoto','ooita','miyazaki','kagoshima','okinawa']
    #csv << ["会社名", "掲載期間", "電話番号",  "住所"]
      begin
      prefecture_names.each do |prefecture_name|

        puts base_url = 'https://townwork.net/' + prefecture_name + '/jc_017/jmc_01702/'

        original_html = open(base_url)
        original_doc = Nokogiri::HTML.parse(original_html)
        #ページの最終地点をとってくる
        size_number  = original_doc.css('.pager-number li').size - 1
        puts max_page_number = original_doc.css('.pager-number li')[size_number].inner_text.to_i

        (1..max_page_number).each do |number|
            url = base_url + '?page=' + number.to_s
            html = open(url)
            doc = Nokogiri::HTML.parse(html)
            doc.css('a.job-lst-main-box-inner.ico-job-lst-main-new-wrap').each do |a|
              open_url = 'https://townwork.net' +  a.attribute('href').value
              open_html = open(open_url)
              open_doc = Nokogiri::HTML.parse(open_html)
              shop_name = open_doc.css('h3.job-detail-ttl-txt').inner_text.split("\n")[2].strip! #店舗名
              pub_period = open_doc.css('.job-age-txt.job-detail-remaining-date-age p').inner_text.strip! #掲載期間
              tel = open_doc.css('p.detail-tel-ttl').inner_text.split("\n")[2].strip! if !open_doc.css.nil? #電話番号
              out_put_tel = tel.split("　")[0].strip if !open_doc.css.nil? #電話番号
              #アドレス出し
              address = ""
              open_doc.css('.job-ditail-tbl-inner').each do |ad|
                #puts ad.css('dt').inner_text
                address = ad.css('  dd').inner_text if ad.css('dt').inner_text == '会社住所'
              end
              address_first = open_doc.css('.breadcrumbs-wrap span')[1].inner_text
              address_second = open_doc.css('.breadcrumbs-wrap span')[2].inner_text.lstrip
              #csv << [shop_name, pub_period, tel, address]
              csv << [shop_name, out_put_tel, address_first,  address_second, address,  "アウト", "架電可", tel, "","美容師",open_url]
            end
          sleep(1)
        end
      end
      rescue => e
        puts e
        next
      end
  end
end



def other_townwork
  CSV.open("/Users/kabushikikaisharijobu/apps/bwj_share/other_townwork_count.csv", "wb") do |csv|
    csv << ["会社名",	"電話番号",	"都道府県",	"市区郡",	"町名・番地",	"アウトorイン",	"リード状況",	"備考",	"リストランク",	"業種（選択）",	"Web サイト"]
    prefecture_names = ['hokkaidou','aomori','iwate','miyagi','akita','yamagata','fukushima','ibaraki','tochigi','gunma','saitama','chiba','tokyo','kanagawa','niigata','toyama','ishikawa','fukui','yamanashi','nagano','gifu','shizuoka','aichi','mie','shiga','kyoto','osaka','hyogo','nara','wakayama','tottori','shimane','okayama','hiroshima','yamaguchi','tokushima','kagawa','ehime','kouchi','fukuoka','saga','nagasaki','kumamoto','ooita','miyazaki','kagoshima','okinawa']
    #csv << ["会社名", "掲載期間", "電話番号",  "住所"]
      begin
      prefecture_names.each do |prefecture_name|

        puts base_url = 'https://townwork.net/' + prefecture_name + '/jc_017/jmc_01703/'

        original_html = open(base_url)
        original_doc = Nokogiri::HTML.parse(original_html)
        #ページの最終地点をとってくる
        size_number  = original_doc.css('.pager-number li').size - 1
        puts max_page_number = original_doc.css('.pager-number li')[size_number].inner_text.to_i

        (1..max_page_number).each do |number|
            url = base_url + '?page=' + number.to_s
            html = open(url)
            doc = Nokogiri::HTML.parse(html)
            doc.css('a.job-lst-main-box-inner.ico-job-lst-main-new-wrap').each do |a|
              open_url = 'https://townwork.net' +  a.attribute('href').value
              open_html = open(open_url)
              open_doc = Nokogiri::HTML.parse(open_html)
              shop_name = open_doc.css('h3.job-detail-ttl-txt').inner_text.split("\n")[2].strip! #店舗名
              pub_period = open_doc.css('.job-age-txt.job-detail-remaining-date-age p').inner_text.strip! #掲載期間
              tel = open_doc.css('p.detail-tel-ttl').inner_text.split("\n")[2].strip! if !open_doc.css.nil? #電話番号
              out_put_tel = tel.split("　")[0].strip if !open_doc.css.nil? #電話番号
              #アドレス出し
              address = ""
              open_doc.css('.job-ditail-tbl-inner').each do |ad|
                #puts ad.css('dt').inner_text
                address = ad.css('  dd').inner_text if ad.css('dt').inner_text == '会社住所'
              end
              address_first = open_doc.css('.breadcrumbs-wrap span')[1].inner_text
              address_second = open_doc.css('.breadcrumbs-wrap span')[2].inner_text.lstrip
              #csv << [shop_name, pub_period, tel, address]
              csv << [shop_name, out_put_tel, address_first,  address_second, address,  "アウト", "架電可", 'エステorネイル' + tel, "","",open_url]
            end
          sleep(1)
        end
      end
      rescue => e
        puts e
        next
      end
  end
end



townwork
#hair_make_townwork
#riyoshi_townwork
#other_townwork
