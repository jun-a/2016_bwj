require 'open-uri'
require 'nokogiri'
require 'date'
require "csv"
require 'json'
require 'open-uri'


def hair_hotpapper
  prefectures = {pre01:"北海道",pre02:"青森県",pre03:"岩手県",pre04:"宮城県",pre05:"秋田県",pre06:"山形県",pre07:"福島県",pre08:"茨城県",pre09:"栃木県",pre10:"群馬県",pre11:"埼玉県",pre12:"千葉県",pre13:"東京都",pre14:"神奈川県",pre15:"新潟県",pre16:"富山県",pre17:"石川県",pre18:"福井県",pre19:"山梨県",pre20:"長野県",pre21:"岐阜県",pre22:"静岡県",pre23:"愛知県",pre24:"三重県",pre25:"滋賀県",pre26:"京都府",pre27:"大阪府",pre28:"兵庫県",pre29:"奈良県",pre30:"和歌山県",pre31:"鳥取県",pre32:"島根県",pre33:"岡山県",pre34:"広島県",pre35:"山口県",pre36:"徳島県",pre37:"香川県",pre38:"愛媛県",pre39:"高知県",pre40:"福岡県",pre41:"佐賀県",pre42:"長崎県",pre43:"熊本県",pre44:"大分県",pre45:"宮崎県",pre46:"鹿児島県",pre47:"沖縄県"}

  prefectures.each do |key,value|
    CSV.open("/Users/kabushikikaisharijobu/apps/bwj_share/all_hotpepper/#{value}_hair_hotpapper.csv", "wb") do |csv|
      csv << ["会社名",	"電話番号",	"都道府県",	"市区郡",	"町名・番地",	"アウトorイン",	"リード状況",	"備考",	"リストランク",	"業種（選択）",	"Web サイト"]

      puts main_url = "https://beauty.hotpepper.jp/#{key}/"
      base_html = open(main_url)
      base_doc = Nokogiri::HTML.parse(base_html)
      puts max_page_number = base_doc.css('p.pa.bottom0.right0').inner_text.split('/')[1].split('ページ')[0].to_i

      base_url = "https://beauty.hotpepper.jp/#{key}/PN"
      (1..max_page_number).each do |number|
        sleep(1)
        url = base_url + number.to_s + '.html'
        html = open(url)
        doc = Nokogiri::HTML.parse(html)
        doc.css('h3.slcHead.cFix a').each do |a|
        begin
          open_url = a.attribute('href').value.split('/?cstt=')[0]
          open_html = open(open_url)
          open_doc = Nokogiri::HTML.parse(open_html)
          shop_name = open_doc.css('p.detailTitle').inner_text#.split#("\n")[1].strip! #店舗名
          address = open_doc.css('ul.fs10')[0].css('li')[0].inner_text.split(value)[1]
          address_first = address.split('市')[0] + '市'#.css('dl.job-ditail-tbl-inner')[2].css('dd p').inner_text #住所
          address_second = address.split('市')[1]#.css('dl.job-ditail-tbl-inner')[2].css('dd p').inner_text #住所
          access = open_doc.css('ul.fs10')[0].css('li')[1].inner_text#.css('dl.job-ditail-tbl-inn er')[2].css('dd p').inner_text #住所
          #こっから電話ページ
          tel_url = open_url + '/tel/'
          open_tel_html = open(tel_url)
          open_tel_doc = Nokogiri::HTML.parse(open_tel_html)
          case value
          when "東京都" then main_address = "東京"
          when "大阪府" then main_address = "大阪"
          when "京都府" then main_address = "京都"
          else  main_address = value.split("県")[0]
          end
          tel = open_tel_doc.css('td.fs16.b').inner_text#.split("\n")[2].strip! #電話番号
          csv << [shop_name, tel, main_address,  address_first, address_second,  "アウト","架電可", access, "","ヘア",open_url]
        rescue => e
          next
        end
        end
      end
    end
  end
end

def eye_hotpapper
  prefectures = {pre01:"北海道",pre02:"青森県",pre03:"岩手県",pre04:"宮城県",pre05:"秋田県",pre06:"山形県",pre07:"福島県",pre08:"茨城県",pre09:"栃木県",pre10:"群馬県",pre11:"埼玉県",pre12:"千葉県",pre13:"東京都",pre14:"神奈川県",pre15:"新潟県",pre16:"富山県",pre17:"石川県",pre18:"福井県",pre19:"山梨県",pre20:"長野県",pre21:"岐阜県",pre22:"静岡県",pre23:"愛知県",pre24:"三重県",pre25:"滋賀県",pre26:"京都府",pre27:"大阪府",pre28:"兵庫県",pre29:"奈良県",pre30:"和歌山県",pre31:"鳥取県",pre32:"島根県",pre33:"岡山県",pre34:"広島県",pre35:"山口県",pre36:"徳島県",pre37:"香川県",pre38:"愛媛県",pre39:"高知県",pre40:"福岡県",pre41:"佐賀県",pre42:"長崎県",pre43:"熊本県",pre44:"大分県",pre45:"宮崎県",pre46:"鹿児島県",pre47:"沖縄県"}

  prefectures.each do |key,value|
    CSV.open("/Users/kabushikikaisharijobu/apps/bwj_share/all_hotpepper/#{value}_hair_hotpapper.csv", "wb") do |csv|
      csv << ["会社名",	"電話番号",	"都道府県",	"市区郡",	"町名・番地",	"アウトorイン",	"リード状況",	"備考",	"リストランク",	"業種（選択）",	"Web サイト"]

      puts main_url = "https://beauty.hotpepper.jp/g-eyelash/#{key}/"
      base_html = open(main_url)
      base_doc = Nokogiri::HTML.parse(base_html)
      puts max_page_number = base_doc.css('p.pa.bottom0.right0').inner_text.split('/')[1].split('ページ')[0].to_i

      base_url = "https://beauty.hotpepper.jp/g-eyelash/#{key}/PN"
      (1..max_page_number).each do |number|
        sleep(1)
        url = base_url + number.to_s + '.html'
        html = open(url)
        doc = Nokogiri::HTML.parse(html)
        doc.css('h3.slcHead.cFix a').each do |a|
        begin
          open_url = a.attribute('href').value.split('/?cstt=')[0]
          open_html = open(open_url)
          open_doc = Nokogiri::HTML.parse(open_html)
          shop_name = open_doc.css('p.detailTitle').inner_text#.split#("\n")[1].strip! #店舗名
          address = open_doc.css('ul.fs10')[0].css('li')[0].inner_text.split(value)[1]
          address_first = address.split('市')[0] + '市'#.css('dl.job-ditail-tbl-inner')[2].css('dd p').inner_text #住所
          address_second = address.split('市')[1]#.css('dl.job-ditail-tbl-inner')[2].css('dd p').inner_text #住所
          access = open_doc.css('ul.fs10')[0].css('li')[1].inner_text#.css('dl.job-ditail-tbl-inn er')[2].css('dd p').inner_text #住所
          #こっから電話ページ
          tel_url = open_url + '/tel/'
          open_tel_html = open(tel_url)
          open_tel_doc = Nokogiri::HTML.parse(open_tel_html)
          case value
          when "東京都" then main_address = "東京"
          when "大阪府" then main_address = "大阪"
          when "京都府" then main_address = "京都"
          else  main_address = value.split("県")[0]
          end
          tel = open_tel_doc.css('td.fs16.b').inner_text#.split("\n")[2].strip! #電話番号
          csv << [shop_name, tel, main_address,  address_first, address_second,  "アウト","架電可", access, "","ヘア",open_url]
        rescue => e
          next
        end
        end
      end
    end
  end
end

def nail_hotpapper
  prefectures = {pre01:"北海道",pre02:"青森県",pre03:"岩手県",pre04:"宮城県",pre05:"秋田県",pre06:"山形県",pre07:"福島県",pre08:"茨城県",pre09:"栃木県",pre10:"群馬県",pre11:"埼玉県",pre12:"千葉県",pre13:"東京都",pre14:"神奈川県",pre15:"新潟県",pre16:"富山県",pre17:"石川県",pre18:"福井県",pre19:"山梨県",pre20:"長野県",pre21:"岐阜県",pre22:"静岡県",pre23:"愛知県",pre24:"三重県",pre25:"滋賀県",pre26:"京都府",pre27:"大阪府",pre28:"兵庫県",pre29:"奈良県",pre30:"和歌山県",pre31:"鳥取県",pre32:"島根県",pre33:"岡山県",pre34:"広島県",pre35:"山口県",pre36:"徳島県",pre37:"香川県",pre38:"愛媛県",pre39:"高知県",pre40:"福岡県",pre41:"佐賀県",pre42:"長崎県",pre43:"熊本県",pre44:"大分県",pre45:"宮崎県",pre46:"鹿児島県",pre47:"沖縄県"}

  prefectures.each do |key,value|
    CSV.open("/Users/kabushikikaisharijobu/apps/bwj_share/all_hotpepper/#{value}_hair_hotpapper.csv", "wb") do |csv|
      csv << ["会社名",	"電話番号",	"都道府県",	"市区郡",	"町名・番地",	"アウトorイン",	"リード状況",	"備考",	"リストランク",	"業種（選択）",	"Web サイト"]

      puts main_url = "https://beauty.hotpepper.jp/g-nail/#{key}/"
      base_html = open(main_url)
      base_doc = Nokogiri::HTML.parse(base_html)
      puts max_page_number = base_doc.css('p.pa.bottom0.right0').inner_text.split('/')[1].split('ページ')[0].to_i

      base_url = "https://beauty.hotpepper.jp/g-nail/#{key}/PN"
      (1..max_page_number).each do |number|
        sleep(1)
        url = base_url + number.to_s + '.html'
        html = open(url)
        doc = Nokogiri::HTML.parse(html)
        doc.css('h3.slcHead.cFix a').each do |a|
        begin
          open_url = a.attribute('href').value.split('/?cstt=')[0]
          open_html = open(open_url)
          open_doc = Nokogiri::HTML.parse(open_html)
          shop_name = open_doc.css('p.detailTitle').inner_text#.split#("\n")[1].strip! #店舗名
          address = open_doc.css('ul.fs10')[0].css('li')[0].inner_text.split(value)[1]
          address_first = address.split('市')[0] + '市'#.css('dl.job-ditail-tbl-inner')[2].css('dd p').inner_text #住所
          address_second = address.split('市')[1]#.css('dl.job-ditail-tbl-inner')[2].css('dd p').inner_text #住所
          access = open_doc.css('ul.fs10')[0].css('li')[1].inner_text#.css('dl.job-ditail-tbl-inn er')[2].css('dd p').inner_text #住所
          #こっから電話ページ
          tel_url = open_url + '/tel/'
          open_tel_html = open(tel_url)
          open_tel_doc = Nokogiri::HTML.parse(open_tel_html)
          case value
          when "東京都" then main_address = "東京"
          when "大阪府" then main_address = "大阪"
          when "京都府" then main_address = "京都"
          else  main_address = value.split("県")[0]
          end
          tel = open_tel_doc.css('td.fs16.b').inner_text#.split("\n")[2].strip! #電話番号
          csv << [shop_name, tel, main_address,  address_first, address_second,  "アウト","架電可", access, "","ヘア",open_url]
        rescue => e
          next
        end
        end
      end
    end
  end
end

def sera_hotpapper
  prefectures = {pre01:"北海道",pre02:"青森県",pre03:"岩手県",pre04:"宮城県",pre05:"秋田県",pre06:"山形県",pre07:"福島県",pre08:"茨城県",pre09:"栃木県",pre10:"群馬県",pre11:"埼玉県",pre12:"千葉県",pre13:"東京都",pre14:"神奈川県",pre15:"新潟県",pre16:"富山県",pre17:"石川県",pre18:"福井県",pre19:"山梨県",pre20:"長野県",pre21:"岐阜県",pre22:"静岡県",pre23:"愛知県",pre24:"三重県",pre25:"滋賀県",pre26:"京都府",pre27:"大阪府",pre28:"兵庫県",pre29:"奈良県",pre30:"和歌山県",pre31:"鳥取県",pre32:"島根県",pre33:"岡山県",pre34:"広島県",pre35:"山口県",pre36:"徳島県",pre37:"香川県",pre38:"愛媛県",pre39:"高知県",pre40:"福岡県",pre41:"佐賀県",pre42:"長崎県",pre43:"熊本県",pre44:"大分県",pre45:"宮崎県",pre46:"鹿児島県",pre47:"沖縄県"}

  prefectures.each do |key,value|
    CSV.open("/Users/kabushikikaisharijobu/apps/bwj_share/all_hotpepper/#{value}_hair_hotpapper.csv", "wb") do |csv|
      csv << ["会社名",	"電話番号",	"都道府県",	"市区郡",	"町名・番地",	"アウトorイン",	"リード状況",	"備考",	"リストランク",	"業種（選択）",	"Web サイト"]

      puts main_url = "https://beauty.hotpepper.jp/relax/#{key}/"
      base_html = open(main_url)
      base_doc = Nokogiri::HTML.parse(base_html)
      puts max_page_number = base_doc.css('p.pa.bottom0.right0').inner_text.split('/')[1].split('ページ')[0].to_i

      base_url = "https://beauty.hotpepper.jp/relax/#{key}/PN"
      (1..max_page_number).each do |number|
        sleep(1)
        url = base_url + number.to_s + '.html'
        html = open(url)
        doc = Nokogiri::HTML.parse(html)
        doc.css('h3.slcHead.cFix a').each do |a|
        begin
          open_url = a.attribute('href').value.split('/?cstt=')[0]
          open_html = open(open_url)
          open_doc = Nokogiri::HTML.parse(open_html)
          shop_name = open_doc.css('p.detailTitle').inner_text#.split#("\n")[1].strip! #店舗名
          address = open_doc.css('ul.fs10')[0].css('li')[0].inner_text.split(value)[1]
          address_first = address.split('市')[0] + '市'#.css('dl.job-ditail-tbl-inner')[2].css('dd p').inner_text #住所
          address_second = address.split('市')[1]#.css('dl.job-ditail-tbl-inner')[2].css('dd p').inner_text #住所
          access = open_doc.css('ul.fs10')[0].css('li')[1].inner_text#.css('dl.job-ditail-tbl-inn er')[2].css('dd p').inner_text #住所
          #こっから電話ページ
          tel_url = open_url + '/tel/'
          open_tel_html = open(tel_url)
          open_tel_doc = Nokogiri::HTML.parse(open_tel_html)
          case value
          when "東京都" then main_address = "東京"
          when "大阪府" then main_address = "大阪"
          when "京都府" then main_address = "京都"
          else  main_address = value.split("県")[0]
          end
          tel = open_tel_doc.css('td.fs16.b').inner_text#.split("\n")[2].strip! #電話番号
          csv << [shop_name, tel, main_address,  address_first, address_second,  "アウト","架電可", access, "","ヘア",open_url]
        rescue => e
          next
        end
        end
      end
    end
  end
end

def eth_hotpapper
  prefectures = {pre01:"北海道",pre02:"青森県",pre03:"岩手県",pre04:"宮城県",pre05:"秋田県",pre06:"山形県",pre07:"福島県",pre08:"茨城県",pre09:"栃木県",pre10:"群馬県",pre11:"埼玉県",pre12:"千葉県",pre13:"東京都",pre14:"神奈川県",pre15:"新潟県",pre16:"富山県",pre17:"石川県",pre18:"福井県",pre19:"山梨県",pre20:"長野県",pre21:"岐阜県",pre22:"静岡県",pre23:"愛知県",pre24:"三重県",pre25:"滋賀県",pre26:"京都府",pre27:"大阪府",pre28:"兵庫県",pre29:"奈良県",pre30:"和歌山県",pre31:"鳥取県",pre32:"島根県",pre33:"岡山県",pre34:"広島県",pre35:"山口県",pre36:"徳島県",pre37:"香川県",pre38:"愛媛県",pre39:"高知県",pre40:"福岡県",pre41:"佐賀県",pre42:"長崎県",pre43:"熊本県",pre44:"大分県",pre45:"宮崎県",pre46:"鹿児島県",pre47:"沖縄県"}

  prefectures.each do |key,value|
    CSV.open("/Users/kabushikikaisharijobu/apps/bwj_share/all_hotpepper/#{value}_hair_hotpapper.csv", "wb") do |csv|
      csv << ["会社名",	"電話番号",	"都道府県",	"市区郡",	"町名・番地",	"アウトorイン",	"リード状況",	"備考",	"リストランク",	"業種（選択）",	"Web サイト"]

      puts main_url = "https://beauty.hotpepper.jp/esthe/#{key}/"
      base_html = open(main_url)
      base_doc = Nokogiri::HTML.parse(base_html)
      puts max_page_number = base_doc.css('p.pa.bottom0.right0').inner_text.split('/')[1].split('ページ')[0].to_i

      base_url = "https://beauty.hotpepper.jp/esthe/#{key}/PN"
      (1..max_page_number).each do |number|
        sleep(1)
        url = base_url + number.to_s + '.html'
        html = open(url)
        doc = Nokogiri::HTML.parse(html)
        doc.css('h3.slcHead.cFix a').each do |a|
        begin
          open_url = a.attribute('href').value.split('/?cstt=')[0]
          open_html = open(open_url)
          open_doc = Nokogiri::HTML.parse(open_html)
          shop_name = open_doc.css('p.detailTitle').inner_text#.split#("\n")[1].strip! #店舗名
          address = open_doc.css('ul.fs10')[0].css('li')[0].inner_text.split(value)[1]
          address_first = address.split('市')[0] + '市'#.css('dl.job-ditail-tbl-inner')[2].css('dd p').inner_text #住所
          address_second = address.split('市')[1]#.css('dl.job-ditail-tbl-inner')[2].css('dd p').inner_text #住所
          access = open_doc.css('ul.fs10')[0].css('li')[1].inner_text#.css('dl.job-ditail-tbl-inn er')[2].css('dd p').inner_text #住所
          #こっから電話ページ
          tel_url = open_url + '/tel/'
          open_tel_html = open(tel_url)
          open_tel_doc = Nokogiri::HTML.parse(open_tel_html)
          case value
          when "東京都" then main_address = "東京"
          when "大阪府" then main_address = "大阪"
          when "京都府" then main_address = "京都"
          else  main_address = value.split("県")[0]
          end
          tel = open_tel_doc.css('td.fs16.b').inner_text#.split("\n")[2].strip! #電話番号
          csv << [shop_name, tel, main_address,  address_first, address_second,  "アウト","架電可", access, "","ヘア",open_url]
        rescue => e
          next
        end
        end
      end
    end
  end
end


hair_hotpapper
eye_hotpapper
nail_hotpapper
sera_hotpapper
eth_hotpapper
