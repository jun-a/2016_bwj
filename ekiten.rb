require 'open-uri'
require 'nokogiri'
require "csv"
require 'json'

def ekiten
  CSV.open("/Users/kabushikikaisharijobu/apps/bwj_share/nagoya_ekiten.csv", "wb") do |csv|
    csv << ["会社名",	"電話番号",	"都道府県",	"市区郡",	"町名・番地",	"アウトorイン",	"リード状況",	"備考",	"リストランク",	"業種（選択）",	"Web サイト"]
    begin
      (1..90).size.times do |number|
        base_url = 'http://www.ekiten.jp/cat_sekkotsu_seikotsu/aichi/index_p' + number.to_s + '.html'
        html = open(base_url) #URLに基づいてhtmlを取得
        doc = Nokogiri::HTML.parse(html) #スクレイピングで取れるように、 HTMLを変換
        doc.css('li.list').each do |a|
          shop_name = a.css('a.shop_name').inner_text.gsub(" ", "")
          tel_url = a.css('a.shop_name').attribute('href').value + 'tel/'
          tel_html = open(tel_url)
          tel_doc = Nokogiri::HTML.parse(tel_html)
          tel = tel_doc.css('span.emphasis_text05').inner_text


          if !a.css('dl.access_info dd')[2].nil?
            location_name = a.css('dl.access_info dd')[2].inner_text.gsub(/(\r|\n|\t|\r\n\t)/, "").gsub('：', '')
            if location_name.include?('県')
              p pre_name = location_name.split('県')[0] + '県'
            else
               pre_name = ''
            end
            if location_name.include?('市')
              p town_name = location_name.split(pre_name)[1].split('市')[0] + '市'
              p under_name = location_name.split(pre_name)[1].split('市')[1].strip
            else
              p town_name = location_name.split(pre_name)[1]
              p under_name = ''
            end
          end

          # p a.css('dl.access_info dd')[0].inner_text.gsub(/(\r|\n|\t|\r\n\t)/, "").gsub('：', '') if !a.css('dl.access_info dd')[0].nil?
          access = a.css('dl.access_info dd')[1].inner_text.gsub(/(\r|\n|\t|\r\n\t)/, "").gsub('：', '') if !a.css('dl.access_info dd')[1].nil?
          csv << [shop_name, tel, pre_name,  town_name, under_name,  "アウト","架電可", access, "","治療",'エキテン']
        end
      end
    rescue
      next
    end
  end
end

def ekiten1
  CSV.open("/Users/kabushikikaisharijobu/apps/bwj_share/nagoya_ekiten2.csv", "wb") do |csv|
    csv << ["会社名",	"電話番号",	"都道府県",	"市区郡",	"町名・番地",	"アウトorイン",	"リード状況",	"備考",	"リストランク",	"業種（選択）",	"Web サイト"]
    begin
      (1..90).size.times do |number|
        base_url = 'http://www.ekiten.jp/cat_esthetics_salon/aichi/index_p' + number.to_s + '.html'
        html = open(base_url) #URLに基づいてhtmlを取得
        doc = Nokogiri::HTML.parse(html) #スクレイピングで取れるように、 HTMLを変換
        doc.css('li.list').each do |a|
          shop_name = a.css('a.shop_name').inner_text.gsub(" ", "")
          tel_url = a.css('a.shop_name').attribute('href').value + 'tel/'
          tel_html = open(tel_url)
          tel_doc = Nokogiri::HTML.parse(tel_html)
          tel = tel_doc.css('span.emphasis_text05').inner_text


          if !a.css('dl.access_info dd')[2].nil?
            location_name = a.css('dl.access_info dd')[2].inner_text.gsub(/(\r|\n|\t|\r\n\t)/, "").gsub('：', '')
            if location_name.include?('県')
              p pre_name = location_name.split('県')[0] + '県'
            else
               pre_name = ''
            end
            if location_name.include?('市')
              p town_name = location_name.split(pre_name)[1].split('市')[0] + '市'
              p under_name = location_name.split(pre_name)[1].split('市')[1].strip
            else
              p town_name = location_name.split(pre_name)[1]
              p under_name = ''
            end
          end

          # p a.css('dl.access_info dd')[0].inner_text.gsub(/(\r|\n|\t|\r\n\t)/, "").gsub('：', '') if !a.css('dl.access_info dd')[0].nil?
          access = a.css('dl.access_info dd')[1].inner_text.gsub(/(\r|\n|\t|\r\n\t)/, "").gsub('：', '') if !a.css('dl.access_info dd')[1].nil?
          csv << [shop_name, tel, pre_name,  town_name, under_name,  "アウト","架電可", access, "","治療",'エキテン']
        end
      end
    rescue
      next
    end
  end
end

ekiten
ekiten1
