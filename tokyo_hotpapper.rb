require 'open-uri'
require 'nokogiri'
require 'date'
require "csv"
require 'json'
require 'open-uri'
#require 'capybara'
#require 'capybara/poltergeist'



def tokyo_hair_hotpapper
  CSV.open("/Users/kabushikikaisharijobu/apps/bwj_share/1shop_tokyo_hair_hotpapper.csv", "wb") do |shop_csv|
    shop_csv << ["店舗名",	"電話番号", "店舗URL",	"住所",	"アクセス",	"営業時間",	"定休日",	"カット価格",	"スタッフ数","エリア","アクセス２"]
  CSV.open("/Users/kabushikikaisharijobu/apps/bwj_share/1stylist_tokyo_hair_hotpapper.csv", "wb") do |stylist_csv|
    stylist_csv << ["スタイリスト名",	"フリガナ",	"経歴",	"詳細ページURL",	"所属店舗名",	"所属店舗URL"]
    #クロール開始
    base_url = 'https://beauty.hotpepper.jp/pre13/PN'
    (1..285).each do |number|
      puts url = base_url + number.to_s + '.html'
      html = open(url)
      doc = Nokogiri::HTML.parse(html)
      doc.css('h3.slcHead.cFix a').each do |a|
      begin
        open_url = a.attribute('href').value.split('/?cstt=')[0]
        open_html = open(open_url)
        open_doc = Nokogiri::HTML.parse(open_html)

        shop_access = open_doc.css('ul.fs10 li')[1].inner_text #★
        area_number = open_doc.css('.pankuzu li').size
        target_area_number = area_number - 2
        area = open_doc.css('.pankuzu li')[target_area_number].inner_text  #★ #.gsub('トップ  >','')[0]


        shop_name = open_doc.css('p.detailTitle').inner_text #★
        address = open_doc.css('td.w618')[1].inner_text #★
        access = open_doc.css('td.w618')[2].inner_text  #★
        time = open_doc.css('td.w618')[3].inner_text  #★
        holiday = open_doc.css('td.w618')[4].inner_text #★
        average_price = open_doc.css('td.w208.vaT')[0].inner_text #★
        staff = open_doc.css('td.w208.vaT')[2].inner_text #★

        #こっから電話ページ
        tel_url = open_url + '/tel/'
        open_tel_html = open(tel_url)
        open_tel_doc = Nokogiri::HTML.parse(open_tel_html)
        tel = open_tel_doc.css('td.fs16.b').inner_text  #★

        #こっからスタイリストページ
        stylist_url = open_url + '/stylist/'
        open_stylist_html = open(stylist_url)
        open_stylist_doc = Nokogiri::HTML.parse(open_stylist_html)
        open_stylist_doc.css('span.numberOfResult').inner_text
        open_stylist_doc.css('td.vaT').each do |i|
          begin
          #puts i
          stylist_name = i.css('p.mT10.fs16.b')[0].css('a').inner_text   #スタイリスト氏名  #★
          stylist_url = i.css('p.mT10.fs16.b')[0].css('a').attribute('href')  #スタイリストURL #★
          stylist_kana = i.css('p.fs10').inner_text #スタイリストふりがな #★
          stylist_history = i.css('.mT5.fs10')[0].inner_text #スタイリスト経歴 #★
          rescue => e
            next
          end
          stylist_csv << [stylist_name,stylist_kana,stylist_history,stylist_url,shop_name,open_url]
        end
        shop_csv << [shop_name,tel,open_url,address,access,time,holiday,average_price,staff,area,shop_access]
        sleep(1)
      rescue => e
        next
      end
      end
    end
  end
  end
end
#tokyo_hair_hotpapper

def itunes_app
  CSV.open("/Users/kabushikikaisharijobu/apps/bwj_share/app_itunes.csv", "wb") do |csv_itunes|
    csv_itunes << ['アプリ名','URL','説明文']
  base_url = 'https://itunes.apple.com/jp/developer/cynd-co.-ltd./id512501162?iPhoneSoftwarePage='
  (1..19).each do |number|
    url = base_url + number.to_s
    html = open(url)
    doc = Nokogiri::HTML.parse(html)
    doc.css('ul.list').each do |i|
      detail_url = i.css('li')[0].css('a').attribute('href').value
      name = i.css('li')[0].inner_text
      detail_html = open(detail_url)
      detail_doc = Nokogiri::HTML.parse(detail_html)
      description = detail_doc.css('p[itemprop="description"]').inner_text
      csv_itunes << [name,detail_url,description]
      sleep(1)
    end
    end
  end
end
itunes_app
