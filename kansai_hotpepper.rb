
require 'open-uri'
require 'nokogiri'
require 'date'
require "csv"
require 'json'
require 'open-uri'
require 'capybara'
require 'capybara/poltergeist'



def osaka_hair_hotpapper
  CSV.open("/Users/kabushikikaisharijobu/apps/bwj_share/osaka_hair_hotpapper.csv", "wb") do |csv|
    csv << ["会社名",	"電話番号",	"都道府県",	"市区郡",	"町名・番地",	"アウトorイン",	"リード状況",	"備考",	"リストランク",	"業種（選択）",	"Web サイト"]
    base_url = 'http://beauty.hotpepper.jp/pre27/PN'
    (1..161).each do |number|
      url = base_url + number.to_s + '.html'
      html = open(url)
      doc = Nokogiri::HTML.parse(html)
      doc.css('h3.slcHead.cFix a').each do |a|
      begin
        open_url = a.attribute('href').value.split('/?cstt=')[0]
        open_html = open(open_url)
        open_doc = Nokogiri::HTML.parse(open_html)
        shop_name = open_doc.css('p.detailTitle').inner_text#.split#("\n")[1].strip! #店舗名
        address = open_doc.css('ul.fs10')[0].css('li')[0].inner_text.split('大阪府')[1]
        address_first = address.split('市')[0] + '市'#.css('dl.job-ditail-tbl-inner')[2].css('dd p').inner_text #住所
        address_second = address.split('市')[1]#.css('dl.job-ditail-tbl-inner')[2].css('dd p').inner_text #住所
        access = open_doc.css('ul.fs10')[0].css('li')[1].inner_text#.css('dl.job-ditail-tbl-inner')[2].css('dd p').inner_text #住所
        #こっから電話ページ
        tel_url = open_url + '/tel/'
        open_tel_html = open(tel_url)
        open_tel_doc = Nokogiri::HTML.parse(open_tel_html)
        tel = open_tel_doc.css('td.fs16.b').inner_text#.split("\n")[2].strip! #電話番号
        csv << [shop_name, tel, "大阪",  address_first, address_second,  "アウト","架電可", access, "","ヘア",open_url]
      rescue => e
        next
      end
      end
    end
  end
end
def osaka_eye_hotpapper
  CSV.open("/Users/kabushikikaisharijobu/apps/bwj_share/osaka_eye_hotpapper.csv", "wb") do |csv|
    csv << ["会社名",	"電話番号",	"都道府県",	"市区郡",	"姓",	"アウトorイン",	"リード状況",	"備考",	"リストランク",	"業種（選択）",	"Web サイト"]
    base_url = 'http://beauty.hotpepper.jp/g-eyelash/pre27/PN'
    (1..40).each do |number|
      url = base_url + number.to_s + '.html'
      html = open(url)
      doc = Nokogiri::HTML.parse(html)
      doc.css('h3.slcHead.cFix a').each do |a|
      begin
        open_url = a.attribute('href').value.split('/?cstt=')[0]
        open_html = open(open_url)
        open_doc = Nokogiri::HTML.parse(open_html)
        shop_name = open_doc.css('p.detailTitle').inner_text#.split#("\n")[1].strip! #店舗名
        address = open_doc.css('ul.fs10')[0].css('li')[0].inner_text.split('大阪府')[1]
        address_first = address.split('市')[0] + '市'#.css('dl.job-ditail-tbl-inner')[2].css('dd p').inner_text #住所
        address_second = address.split('市')[1]#.css('dl.job-ditail-tbl-inner')[2].css('dd p').inner_text #住所
        access = open_doc.css('ul.fs10')[0].css('li')[1].inner_text#.css('dl.job-ditail-tbl-inner')[2].css('dd p').inner_text #住所
        #こっから電話ページ
        tel_url = open_url + '/tel/'
        open_tel_html = open(tel_url)
        open_tel_doc = Nokogiri::HTML.parse(open_tel_html)
        tel = open_tel_doc.css('td.fs16.b').inner_text#.split("\n")[2].strip! #電話番号
        csv << [shop_name, tel, "大阪",  address_first, address_second,  "アウト","架電可", access, "","アイ",open_url]
      rescue => e
        next
      end
      end
    end
  end
end
def osaka_nail_hotpapper
  CSV.open("/Users/kabushikikaisharijobu/apps/bwj_share/osaka_nail_hotpapper.csv", "wb") do |csv|
    csv << ["会社名",	"電話番号",	"都道府県",	"市区郡",	"姓",	"アウトorイン",	"リード状況",	"備考",	"リストランク",	"業種（選択）",	"Web サイト"]
    base_url = 'http://beauty.hotpepper.jp/g-nail/pre27/PN'
    (1..55).each do |number|
      url = base_url + number.to_s + '.html'
      html = open(url)
      doc = Nokogiri::HTML.parse(html)
      doc.css('h3.slcHead.cFix a').each do |a|
      begin
        open_url = a.attribute('href').value.split('/?cstt=')[0]
        open_html = open(open_url)
        open_doc = Nokogiri::HTML.parse(open_html)
        shop_name = open_doc.css('p.detailTitle').inner_text#.split#("\n")[1].strip! #店舗名
        address = open_doc.css('ul.fs10')[0].css('li')[0].inner_text.split('大阪府')[1]
        address_first = address.split('市')[0] + '市'#.css('dl.job-ditail-tbl-inner')[2].css('dd p').inner_text #住所
        address_second = address.split('市')[1]#.css('dl.job-ditail-tbl-inner')[2].css('dd p').inner_text #住所
        access = open_doc.css('ul.fs10')[0].css('li')[1].inner_text#.css('dl.job-ditail-tbl-inner')[2].css('dd p').inner_text #住所
        #こっから電話ページ
        tel_url = open_url + '/tel/'
        open_tel_html = open(tel_url)
        open_tel_doc = Nokogiri::HTML.parse(open_tel_html)
        tel = open_tel_doc.css('td.fs16.b').inner_text#.split("\n")[2].strip! #電話番号
        csv << [shop_name, tel, "大阪",  address_first, address_second,  "アウト","架電可", access, "","ネイル",open_url]
      rescue => e
        next
      end
      end
    end
  end
end
def osaka_sera_hotpapper
  CSV.open("/Users/kabushikikaisharijobu/apps/bwj_share/osaka_sera_hotpapper.csv", "wb") do |csv|
    csv << ["会社名",	"電話番号",	"都道府県",	"市区郡",	"姓",	"アウトorイン",	"リード状況",	"備考",	"リストランク",	"業種（選択）",	"Web サイト"]
    base_url = 'http://beauty.hotpepper.jp/relax/pre27/PN'
    (1..66).each do |number|
      url = base_url + number.to_s + '.html'
      html = open(url)
      doc = Nokogiri::HTML.parse(html)
      doc.css('h3.slcHead.cFix a').each do |a|
      begin
        open_url = a.attribute('href').value.split('/?cstt=')[0]
        open_html = open(open_url)
        open_doc = Nokogiri::HTML.parse(open_html)
        shop_name = open_doc.css('p.detailTitle').inner_text#.split#("\n")[1].strip! #店舗名
        address = open_doc.css('ul.fs10')[0].css('li')[0].inner_text.split('大阪府')[1]
        address_first = address.split('市')[0] + '市'#.css('dl.job-ditail-tbl-inner')[2].css('dd p').inner_text #住所
        address_second = address.split('市')[1]#.css('dl.job-ditail-tbl-inner')[2].css('dd p').inner_text #住所
        access = open_doc.css('ul.fs10')[0].css('li')[1].inner_text#.css('dl.job-ditail-tbl-inner')[2].css('dd p').inner_text #住所
        #こっから電話ページ
        tel_url = open_url + '/tel/'
        open_tel_html = open(tel_url)
        open_tel_doc = Nokogiri::HTML.parse(open_tel_html)
        tel = open_tel_doc.css('td.fs16.b').inner_text#.split("\n")[2].strip! #電話番号
        csv << [shop_name, tel, "大阪",  address_first, address_second,  "アウト","架電可", access, "","セラ",open_url]
      rescue => e
        next
      end
      end
    end
  end
end
def osaka_eth_hotpapper
  CSV.open("/Users/kabushikikaisharijobu/apps/bwj_share/osaka_eth_hotpapper.csv", "wb") do |csv|
    csv << ["会社名",	"電話番号",	"都道府県",	"市区郡",	"姓",	"アウトorイン",	"リード状況",	"備考",	"リストランク",	"業種（選択）",	"Web サイト"]
    base_url = 'http://beauty.hotpepper.jp/esthe/pre27/PN'
    (1..67).each do |number|
      url = base_url + number.to_s + '.html'
      html = open(url)
      doc = Nokogiri::HTML.parse(html)
      doc.css('h3.slcHead.cFix a').each do |a|
      begin
        open_url = a.attribute('href').value.split('/?cstt=')[0]
        open_html = open(open_url)
        open_doc = Nokogiri::HTML.parse(open_html)
        shop_name = open_doc.css('p.detailTitle').inner_text#.split#("\n")[1].strip! #店舗名
        address = open_doc.css('ul.fs10')[0].css('li')[0].inner_text.split('大阪府')[1]
        address_first = address.split('市')[0] + '市'#.css('dl.job-ditail-tbl-inner')[2].css('dd p').inner_text #住所
        address_second = address.split('市')[1]#.css('dl.job-ditail-tbl-inner')[2].css('dd p').inner_text #住所
        access = open_doc.css('ul.fs10')[0].css('li')[1].inner_text#.css('dl.job-ditail-tbl-inner')[2].css('dd p').inner_text #住所
        #こっから電話ページ
        tel_url = open_url + '/tel/'
        open_tel_html = open(tel_url)
        open_tel_doc = Nokogiri::HTML.parse(open_tel_html)
        tel = open_tel_doc.css('td.fs16.b').inner_text#.split("\n")[2].strip! #電話番号
        csv << [shop_name, tel, "大阪",  address_first, address_second,  "アウト","架電可", access, "","エステ",open_url]
      rescue => e
        next
      end
      end
    end
  end
end



#osaka_hair_hotpapper
osaka_eye_hotpapper
osaka_nail_hotpapper
osaka_sera_hotpapper
osaka_eth_hotpapper
