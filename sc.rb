require 'open-uri'
require 'nokogiri'
require 'date'
require "csv"
require 'json'
require 'open-uri'


def flesh_page
  CSV.open("/Users/kabushikikaisharijobu/apps/bwj_share/wantedly_count_flesh.csv", "wb") do |csv|
    csv << ["案件名", "案件URL", "掲載開始日",  "通算掲載日数", "トータルシェア数", "トータルのPV数","Dailyでの平均シェア数","Dailyでの平均PV数"]
    #新着一覧取得用スクリプト
    base_url = 'https://www.wantedly.com/projects/recent?fresh=true&occupations%5B%5D=engineer&occupations%5B%5D=web_engineer&occupations%5B%5D=mobile_engineer&occupations%5B%5D=infra_engineer&occupations%5B%5D=others_engineer&occupations%5B%5D=designer&occupations%5B%5D=ui_designer&occupations%5B%5D=graphic_designer&occupations%5B%5D=others_designer&occupations%5B%5D=director&occupations%5B%5D=corporate_staff&occupations%5B%5D=sales&occupations%5B%5D=marketing&occupations%5B%5D=writer&occupations%5B%5D=others'

    (1..10).each do |number|
      url = base_url + '&page=' + number.to_s
      #各ページの案件URL取得
      html = open(url)
      doc = Nokogiri::HTML.parse(html)

      doc.css('h1.project-title a').each do |anchor|
        begin
          page_url = 'https://www.wantedly.com' + anchor[:href].split('?')[0]
          page_html = open(page_url)
          page_doc = Nokogiri::HTML.parse(page_html)
          page_title = page_doc.css('title').text.split('- Wantedly')[0] #案件名の取得
          pub_date_text = page_doc.css('.published-date').text.strip #掲載開始日
          pub_date = Date.parse(pub_date_text)
          pub_period = (Date.today - pub_date + 1).to_s.split('/')[0].to_i #掲載期間
          total_pv = page_doc.css('.total-views').text.strip.to_i #pv数
          daily_pv = (total_pv/pub_period)#.to_s + ' pv/daily'
          total_share_count = JSON.load(open("http://graph.facebook.com/" + page_url))["shares"]
          daily_share_count = (total_share_count/pub_period)
          csv << [page_title, page_url, pub_date_text, pub_period, total_share_count, total_pv, daily_share_count, daily_pv]
        rescue => e
          p e
        end
        sleep(10)
        end
    end

  end
end


def popular_page
  CSV.open("/Users/kabushikikaisharijobu/apps/bwj_share/wantedly_count_popular.csv", "wb") do |csv|
    csv << ["案件名", "案件URL", "掲載開始日",  "通算掲載日数", "トータルシェア数", "トータルのPV数","Dailyでの平均シェア数","Dailyでの平均PV数"]
    #新着一覧取得用スクリプト
    base_url = 'https://www.wantedly.com/projects/popular?fresh=true&occupations%5B%5D=engineer&occupations%5B%5D=web_engineer&occupations%5B%5D=mobile_engineer&occupations%5B%5D=infra_engineer&occupations%5B%5D=others_engineer&occupations%5B%5D=designer&occupations%5B%5D=ui_designer&occupations%5B%5D=graphic_designer&occupations%5B%5D=others_designer&occupations%5B%5D=director&occupations%5B%5D=corporate_staff&occupations%5B%5D=sales&occupations%5B%5D=marketing&occupations%5B%5D=writer&occupations%5B%5D=others'
    (1..10).each do |number|
      url = base_url + '&page=' + number.to_s
      #各ページの案件URL取得
      html = open(url)
      doc = Nokogiri::HTML.parse(html)

      doc.css('h1.project-title a').each do |anchor|
      begin
        page_url = 'https://www.wantedly.com' + anchor[:href].split('?')[0]
        page_html = open(page_url)
        page_doc = Nokogiri::HTML.parse(page_html)
        page_title = page_doc.css('title').text.split('- Wantedly')[0] #案件名の取得
        pub_date_text = page_doc.css('.published-date').text.strip #掲載開始日
        pub_date = Date.parse(pub_date_text)
        pub_period = (Date.today - pub_date + 1).to_s.split('/')[0].to_i #掲載期間
        total_pv = page_doc.css('.total-views').text.strip.to_i #pv数
        daily_pv = (total_pv/pub_period)#.to_s + ' pv/daily'
        total_share_count = JSON.load(open("http://graph.facebook.com/" + page_url))["shares"]
        daily_share_count = (total_share_count/pub_period)
        csv << [page_title, page_url, pub_date_text, pub_period, total_share_count, total_pv, daily_share_count, daily_pv]
      rescue => e
        p e
      end
      sleep(10)
      end
    end

  end
end

flesh_page
#popular_page
